<?php

namespace Tests\Unit\Products;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductVariation;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * It_uses_the_slug_for_the_route_key_name
     *
     * @test
     * @return void
     */
    public function it_uses_the_slug_for_the_route_key_name()
    {
        $product = new Product();

        $this->assertEquals($product->getRouteKeyName(), 'slug');
    }

    /**
     * It has many categories
     *
     * @test
     * @return void
     **/
    public function it_has_many_categories()
    {
        $product = Product::factory()->create();

        $product->categories()->save(Category::factory()->create());

        $this->assertInstanceOf(
            Category::class,
            $product->categories()->first()
        );
    }

    /**
     * It has many variations
     *
     * @test
     * @return void
     **/
    public function it_has_many_variations()
    {
        $product = Product::factory()->create();

        $product->variations()->save(ProductVariation::factory()->create());

        $this->assertInstanceOf(
            ProductVariation::class,
            $product->variations->first()
        );
    }
}
