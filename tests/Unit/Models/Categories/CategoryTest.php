<?php

namespace Tests\Unit\Models\Categories;

use App\Models\Category;
use App\Models\Product;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    /**
     * It has many children
     *
     * @test
     * @return void
     */
    public function it_has_many_children()
    {
        $category = Category::factory()->create();

        $category->children()->save(Category::factory()->makeOne());

        $this->assertInstanceOf(Category::class, $category->children->first());
    }

    /**
     * It can fetch only parents
     *
     * @test
     * @return void
     */
    public function it_can_fetch_only_parents()
    {
        $category = Category::factory()->create();

        $category->children()->save(Category::factory()->makeOne());

        $this->assertEquals(1, Category::parents()->count());
    }

    /**
     * It is orderable by a numbered order
     *
     * @test
     * @return void
     */
    public function it_is_orderable_by_a_numbered_order()
    {
        Category::factory()->create([
            "order" => 2,
        ]);

        $anotherCategory = Category::factory()->create([
            "order" => 1,
        ]);

        $this->assertEquals(
            $anotherCategory->name,
            Category::ordered()->first()->name
        );
    }

    /**
     * It has many products
     *
     * @test
     * @return void
     */
    public function it_has_many_products()
    {
        $category = Category::factory()->create();

        $category->products()->save(Product::factory()->create());

        $this->assertInstanceOf(Product::class, $category->products->first());
    }
}
