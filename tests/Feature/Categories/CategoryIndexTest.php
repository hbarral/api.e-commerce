<?php

namespace Tests\Feature\Categories;

// use Illuminate\Foundation\Testing\RefreshDatabase;
// use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Category;
use Tests\TestCase;

class CategoryIndexTest extends TestCase
{
    /**
     * It returns a collection of categories
     *
     * @test
     * @return void
     */
    public function it_returns_a_collection_of_categories()
    {
        $categories = Category::factory(2)->create();

        $response = $this->json('GET', 'api/categories');

        $categories->each(function ($category) use ($response) {
            $response->assertJsonFragment([
                'slug' => $category->slug,
            ]);
        });
    }

    /**
     * It returns only parent categories
     *
     * @test
     * @return void
     */
    public function it_returns_only_parent_categories()
    {
        $category = Category::factory()->create();

        $category->children()->save(
            Category::factory()->create()
        );

        $this->json('GET', 'api/categories')
             ->assertJsonCount(1, 'data');
    }

    /**
     * It returns categories ordered by their given order
     *
     * @test
     * @return void
     */
    public function it_returns_categories_ordered_by_their_given_order()
    {
        $category = Category::factory()->create([
            'order' => 2,
        ]);

        $anotherCategory = Category::factory()->create([
            'order' => 1,
        ]);

        $this->json('GET', 'api/categories')
             ->assertSeeInOrder([
                 $anotherCategory->slug,
                 $category->slug,
             ]);
    }
}
