<?php

namespace Tests\Feature\Products;

use App\Models\Product;
// use Illuminate\Foundation\Testing\RefreshDatabase;
// use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductIndexTest extends TestCase
{
    /**
     * It shows a collection of products
     *
     * @test
     * @return void
     */
    public function it_shows_a_collection_of_products()
    {
        $product = Product::factory()->create();

        $this->json('GET', 'api/products')
             ->assertJsonFragment([
                 'id' => $product->id,
             ]);
    }

    /**
     * It has paginated data
     *
     * @test
     * @return void
     */
    public function it_has_paginated_data()
    {
        // $product = Product::factory()->create();

        $this->json('GET', 'api/products')
             ->assertJsonStructure([
                 'meta',
             ]);
    }
}
