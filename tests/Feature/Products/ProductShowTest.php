<?php

namespace Tests\Feature\Products;

// use Illuminate\Foundation\Testing\RefreshDatabase;
// use Illuminate\Foundation\Testing\WithFaker;

use App\Models\Product;
use Tests\TestCase;

class ProductShowTest extends TestCase
{
    /**
     * It fails if a product cant be found
     *
     * @test
     * @return void
     */
    public function it_fails_if_a_product_cant_be_found()
    {
        $this->json('GET', 'api/products/nope')
             ->assertStatus(404);
    }

    /**
     * It shows a product
     *
     * @test
     * @return void
     */
    public function it_shows_a_product()
    {
        $product = Product::factory()->create();

        $this->json('GET', "api/products/{$product->slug}")
             ->assertJsonFragment([
                 'id' => $product->id,
             ]);
    }
}
