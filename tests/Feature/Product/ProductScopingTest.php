<?php

namespace Tests\Feature\Product;

// use Illuminate\Foundation\Testing\RefreshDatabase;
// use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Product;
use App\Models\Category;

class ProductScopingTest extends TestCase
{
    /**
     * It can scope by category
     *
     * @test
     * @return void
     */
    public function it_can_scope_by_category()
    {
        $product = Product::factory()->create();

        $product->categories()->save(
            $category = Category::factory()->create()
        );
        
        Product::category()->create();

        $this->json('GET', "api/products?category={$category->slug}")
             ->assertJsonCount(1, 'data');
    }
}
